import { MixOperation } from './node_modules/three/build/three.module.js';
import * as THREE from './node_modules/three/build/three.module.js';


import { OrbitControls } from './node_modules/three/examples/jsm/controls/OrbitControls.js'

import { GLTFLoader} from './node_modules/three/examples/jsm/loaders/GLTFLoader.js';


const textureLoader = new THREE.TextureLoader()

const normalTexture = textureLoader.load('/textures/NormalMap.png')
const normalTexture2 = textureLoader.load('/textures/NormalMap2.png')



const scene = new THREE.Scene();

const camera = new THREE.PerspectiveCamera(70, window.innerWidth / window.innerHeight, 0.1, 1000);

scene.background = new THREE.Color(0xffffff);
const renderer = new THREE.WebGLRenderer({
    canvas: document.querySelector('#bg'),
})


const gridHelper = new THREE.GridHelper(200,50);
// scene.add(gridHelper)
renderer.setPixelRatio(window.devicePixelRatio);
renderer.setSize(window.innerWidth, window.innerHeight);
camera.position.setZ(75);

renderer.render(scene, camera);
// Objects
const geometry = new THREE.SphereBufferGeometry(10, 64, 64)
const pGeometry = new THREE.SphereBufferGeometry(5, 64, 64)
const pGeometry2 = new THREE.SphereBufferGeometry(7, 64, 64)

// ajouter objet


const loader = new GLTFLoader();
loader.load(
    
    'model/scene.gltf',
    function(gltf){
        gltf.scene.scale.set(23,25,23)

        
window.addEventListener('wheel', mousewheel)

        function mousewheel(event){
            let deltaty = event.deltaY
            positionRotation += deltaty*0.0000001
            // console.log(positionRotation)
            if (positionRotation > 5) {
                gltf.scene.rotation.y = 5 *0.2
            } else if (positionRotation < -5){
                gltf.scene.rotation.y = -5 *0.2
            } else {
                gltf.scene.rotation.y = positionRotation *0.2
            }


        
        }
        scene.add(gltf.scene);

    }
)


const controls = new OrbitControls( camera, renderer.domElement );
controls.update();

// Materials

const material = new THREE.MeshStandardMaterial()
material.metalness = 7
material.roughness = 0.5
material.normalMap = normalTexture;

material.color = new THREE.Color(0x292929)

// Mesh
const sphere = new THREE.Mesh(geometry,material)
//scene.add(sphere)


const paterial = new THREE.MeshStandardMaterial({color: 0x5e1701})
paterial.metalness = 0.5
paterial.roughness = 0.2
paterial.normalMap = normalTexture;

const projectSphere = new THREE.Mesh(pGeometry, paterial)
projectSphere.position.x = 30
projectSphere.position.y = -5
projectSphere.position.z = -25


scene.add(projectSphere)


const paterial2 = new THREE.MeshStandardMaterial({  color: 0x5e1701 })
paterial2.metalness = 0.5
paterial2.roughness = 0.1
paterial2.normalMap = normalTexture;

const projectSphere2 = new THREE.Mesh(pGeometry2, paterial2)
projectSphere2.position.x = -55
projectSphere2.position.y = -5
projectSphere2.position.z = -25


scene.add(projectSphere2)


const pointLight2 = new THREE.DirectionalLight(0xfdfdfd, 2000)
pointLight2.position.set(-50,-100,-10)
pointLight2.intensity = 2
scene.add(pointLight2)
const sphereSize = 1;
const pointLightHelper2 = new THREE.PointLightHelper( pointLight2, sphereSize );
scene.add( pointLightHelper2 );

const pointLight3 = new THREE.DirectionalLight(0xfc4903, 2000)
pointLight3.position.set(100,100,100)
pointLight3.intensity = 0.5
scene.add(pointLight3)
const sphereSize2 = 1;
const pointLightHelper3 = new THREE.PointLightHelper( pointLight3, sphereSize2 );
scene.add( pointLightHelper3 )




function addStar() {
    const geometry = new THREE.SphereGeometry(.7, 24, 24);
    geometry.scale(12,12,12)
    const material = new THREE.MeshStandardMaterial( { color: 0x5e1701 } )
    material.metalness = 0.5
    material.roughness = 0.2
    material.normalMap = normalTexture;
    const star = new THREE.Mesh( geometry, material );

    //let clock = clock.elapsedTime();
    const [x, y, z] = Array(3).fill().map(() => THREE.MathUtils.randFloatSpread(500) );
    star.position.set(x, y, z);
    //scene.add(star)
}
Array(20).fill().forEach(addStar)

const ft = new THREE.TextureLoader().load("skybox/gloomy_ft.png")
const bk = new THREE.TextureLoader().load("skybox/gloomy_bk.png")
const up = new THREE.TextureLoader().load("skybox/gloomy_up.png")
const dn = new THREE.TextureLoader().load("skybox/gloomy_dn.png")
const rt = new THREE.TextureLoader().load("skybox/gloomy_rt.png")
const lf = new THREE.TextureLoader().load("skybox/gloomy_lf.png")


const sky = new THREE.CubeTextureLoader();
const textureSky = sky.load([
    "skybox/weltraum.png",
    "skybox/weltraumh.png",
    "skybox/weltrauml.png",
    "skybox/weltraumr.png",
    "skybox/weltraumr.png",
    "skybox/weltraumr.png"
])
const spaceTexture = new THREE.TextureLoader().load('bg.jpeg');
scene.background = textureSky


function animate() {
    requestAnimationFrame( animate );
    //controls.update();
    renderer.render(scene, camera);
}
 animate();
 sphere.addEventListener('click', function(e) {
    window.alert('Click sur moi');
})
 document.addEventListener('mousemove', onDocumentMouseMove)
    
 let mouseX = 0
 let mouseY = 0
 
 let targetX = 0
 let targetY = 0
 
 const windowX = window.innerWidth / 2;
 const windowY = window.innerHeight / 2;
 
 function onDocumentMouseMove(event) {
     mouseX = (event.clientX - windowX)
     mouseY = (event.clientY - windowY)
 }
 
 //enlever les barre scroll
 //window.addEventListener('scroll', updateSphere);
 const clock = new THREE.Clock()

 // fonction pour bouger la camera
let y = 0;
let positionRotation = 0;

window.addEventListener('wheel', mousewheel)
function mousewheel(event){
    var x = camera.position.x,
    y = camera.position.y,
    z = camera.position.z;

    let deltaty = event.deltaY
    positionRotation += deltaty*0.005



    if(positionRotation > 5){
        camera.position.x = 5;
        scene.position.y = 5 *3
    }else if(positionRotation < -5)
    {
        camera.position.x = -5;
        scene.position.y = -5 *3
    } else {
        camera.position.x = positionRotation;
        scene.position.y = positionRotation *3
    }
    // console.log(Math.cos(positionRotation) - z * Math.sin(positionRotation) )
    //camera.position.z = z * Math.cos(positionRotation) + x * Math.sin(positionRotation);

    camera.lookAt(scene.position);


}
 const tick = () =>
 {
 
     targetX = mouseX * .001
     targetY = mouseY * .001
     const elapsedTime = clock.getElapsedTime()
     console.log(elapsedTime)
     scene.rotation.y = elapsedTime *0.02
     projectSphere.rotation.y = elapsedTime *0.6
     // Update Orbital Controls
     // controls.update()
 

     // updatre camera position
     // Render
     renderer.render(scene, camera)
 
     // Call tick again on the next frame
     window.requestAnimationFrame(tick)
 }
 
 tick()


