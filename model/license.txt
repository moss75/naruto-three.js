Model Information:
* title:	Naruto (Baryon)
* source:	https://sketchfab.com/3d-models/naruto-baryon-d22d1ba3aca94e8daa61e88dac5cc2fe
* author:	MontanariArt (https://sketchfab.com/montanariart)

Model License:
* license type:	CC-BY-4.0 (http://creativecommons.org/licenses/by/4.0/)
* requirements:	Author must be credited. Commercial use is allowed.

If you use this 3D model in your project be sure to copy paste this credit wherever you share it:
This work is based on "Naruto (Baryon)" (https://sketchfab.com/3d-models/naruto-baryon-d22d1ba3aca94e8daa61e88dac5cc2fe) by MontanariArt (https://sketchfab.com/montanariart) licensed under CC-BY-4.0 (http://creativecommons.org/licenses/by/4.0/)